# Set the base image
FROM php:8.0-fpm-alpine

# Update and install packages
RUN apk update && apk add \
    git \
    zip \
    unzip \
    curl \
    postgresql-dev \
    oniguruma-dev \
    libxml2-dev \
    libzip-dev \
    libpng-dev \
    libjpeg-turbo-dev \
    freetype-dev \
    sqlite-dev

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mysqli pdo_pgsql opcache mbstring xml zip gd pdo_sqlite

# Set working directory
WORKDIR /var/www/html

# Copy the application files
COPY . .

# Set permissions
RUN chown -R www-data:www-data /var/www/html
RUN chmod -R 777 /var

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install project dependencies
# RUN composer install
# Install EasyAdmin demo project
# RUN composer create-project easyCorp/easyadmin-demo my_project

# Expose port
EXPOSE 9000

# Start PHP-FPM
CMD ["php-fpm"]
